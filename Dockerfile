FROM alpine:latest AS base
RUN apk upgrade --update-cache && \
apk add --no-cache \
  make \
  git \
  curl \
  jq \
  openssl \
  docker \
  gcc \
  python3 \
  py3-pip \
  python3-dev \
  g++ \
  libffi-dev \
  openssl-dev
RUN pip3 install --no-cache-dir --upgrade \
  pip \
  setuptools \
  awscli
RUN pip3 install --no-cache-dir \
  docker-compose

FROM base AS dind
ARG DOCKER_HOST="tcp://localhost:2375"
ENV DOCKER_HOST="${DOCKER_HOST}"
