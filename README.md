# Gitlab CI Runner (for k8s Executor)
Runner image for usage in Gitlab CI pipelines that accounts for use within a Kubernetes executor type of runner coordinator.

[![pipeline status](https://gitlab.com/mycf.sg/img-gitlab-ci-runner-k8s/badges/master/pipeline.svg)](https://gitlab.com/mycf.sg/img-gitlab-ci-runner-k8s/commits/master)

[![Use image](https://img.shields.io/badge/Public%20ECR-mycfsg%2Fgitlab--ci--runner--k8s-blue.svg)](https://gallery.ecr.aws/mycfsg/gitlab-ci-runner-k8s)

[![Use `dind`-compatible image](https://img.shields.io/badge/Public%20ECR-mycfsg%2Fgitlab--ci--runner--k8s--dind-blue.svg)](https://gallery.ecr.aws/mycfsg/gitlab-ci-runner-k8s-dind)

# Development

## Version Bumping
To bump a version, [run a pipeline](https://gitlab.com/mycf.sg/img-gitlab-ci-runner-k8s/pipelines/new) with the input variable `VERSION_BUMP` set to one of `"major"` or `"minor"`. It defauilts to `"patch"`.

# Re-Use

## Pipeline Configuration

| Pipeline Variable | Description | Example Value |
| --- | --- | -- |
| `SSH_DEPLOY_KEY` | Base64 encoded private key corresponding to a registered Deploy Key (see below section on Deploy Key Setup) | `null` |

## Deploy Key Setup
Generate a private/public key-pair using:

```bash
make keys
```

The keys will be placed in a `.ssh` directory relative to your present working directory. Run the private key through a base64 encoding and copy the output content:

```bash
cd .ssh && \
  cat ./deploy_rsa \
    | base64 -w 0 \
    > ./deploy_rsa.b64;
cat ./deploy_rsa.b64 | pbcopy;
```

Paste the value in the pipeline variable `SSH_DEPLOY_KEY`.

Copy the contents of the public key (`./deploy_rsa.pub`) and paste the value in the Deploy Keys section of the CI/CD->Repository settings page in your repository.

# License
This project is licensed under [the MIT license](./LICENSE).
