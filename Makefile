IMAGE_REF=mycfsg/gitlab-ci-runner-k8s
IMAGE_DIND_REF=mycfsg/gitlab-ci-runner-k8s-dind
ECR_IMAGE_REF=public.ecr.aws/mycfsg/gitlab-ci-runner-k8s
ECR_IMAGE_DIND_REF=public.ecr.aws/mycfsg/gitlab-ci-runner-k8s-dind

-include ./scripts/utils/Makefile

build:
	@$(MAKE) _build TARGET=base TAG=$(IMAGE_REF):latest
build.dind:
	@$(MAKE) _build TARGET=dind TAG=$(IMAGE_DIND_REF):latest
_build:
	@docker build \
		--target=${TARGET} \
		--tag ${TAG} \
		.

test: build
	@$(MAKE) _test ARGS="make --version"
	@$(MAKE) _test ARGS="git version"
	@$(MAKE) _test ARGS="curl --version"
	@$(MAKE) _test ARGS="jq --version"
	@$(MAKE) _test ARGS="openssl version"
	@$(MAKE) _test ARGS="docker --version"
	@$(MAKE) _test ARGS="gcc --version"
	@$(MAKE) _test ARGS="python3 --version"
	@$(MAKE) _test ARGS="g++ --version"
	@$(MAKE) _test ARGS="docker-compose --version"
_test:
	@docker run $(IMAGE_REF):latest ${ARGS}

publish: build build.dind
	@docker run $(IMAGE_REF):latest cat /etc/alpine-release > ./.v.alpine
	@docker run $(IMAGE_REF):latest docker --version | cut -f 3 -d ' ' | sed -e 's/,//g' > ./.v.docker
	@docker run $(IMAGE_REF):latest docker-compose --version | cut -f 3 -d ' ' | sed -e 's/,//g' > ./.v.docker-compose
	@date +'%Y%m%d' > ./.v.date
	@$(MAKE) _publish FROM_TAG="$(IMAGE_REF):latest" TAG="$(ECR_IMAGE_REF):latest"
	@$(MAKE) _publish FROM_TAG="$(IMAGE_REF):latest" TAG="$(ECR_IMAGE_REF):alpine-$$(cat ./.v.alpine)_docker-compose-$$(cat ./.v.docker-compose)_docker-$$(cat ./.v.docker)"
	@$(MAKE) _publish FROM_TAG="$(IMAGE_REF):latest" TAG="$(ECR_IMAGE_REF):$$(cat ./.v.date)"
	@$(MAKE) _publish FROM_TAG="$(IMAGE_DIND_REF):latest" TAG="$(ECR_IMAGE_DIND_REF):latest"
	@$(MAKE) _publish FROM_TAG="$(IMAGE_DIND_REF):latest" TAG="$(ECR_IMAGE_DIND_REF):alpine-$$(cat ./.v.alpine)_docker-compose-$$(cat ./.v.docker-compose)_docker-$$(cat ./.v.docker)"
	@$(MAKE) _publish FROM_TAG="$(IMAGE_DIND_REF):latest" TAG="$(ECR_IMAGE_DIND_REF):$$(cat ./.v.date)"
_publish:
	@echo ${FROM_TAG} ${TAG}
	@docker tag ${FROM_TAG} ${TAG}
	@$(MAKE) log.info MSG="Publishing ${TAG}..."
	@docker push ${TAG}
